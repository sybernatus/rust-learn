use std::collections::HashMap;
use axum::http::Method;
use google_drive3::{DriveHub, hyper::Client, hyper_rustls, oauth2 as gdrive_oauth2};
use http_body_util::Full;
use hyper::{Request, Response, StatusCode};
use hyper::body::{Body, Bytes, Incoming};
use hyper::http::Error;
use log::info;

pub async fn routing(request: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {

    match request.method() {
        &Method::GET => get(request).await,
        // &Method::POST => post(request).await,
        _ => Ok(Response::builder().body(Full::new(Bytes::from("not found"))).unwrap()),
    }

}

async fn get(_request: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {
    info!("google_drive3 request");
    let query = if let Some(q) = _request.uri().query() {
        q
    } else {
        return Ok(Response::builder()
            .status(StatusCode::UNPROCESSABLE_ENTITY)
            .body(Full::new(Bytes::from("query not found")))
            .unwrap());
    };
    let params = form_urlencoded::parse(query.as_bytes())
        .into_owned()
        .collect::<HashMap<String, String>>();
    let access_token = if let Some(p) = params.get("access_token") {
        p.as_str().to_string()
    } else {
        return Ok(Response::builder()
            .status(StatusCode::UNPROCESSABLE_ENTITY)
            .body(Full::new(Bytes::from("access_token not found")))
            .unwrap());
    };
    let auth = gdrive_oauth2::AccessTokenAuthenticator::builder(access_token).build().await.unwrap();
    let hub = DriveHub::new(
        Client::builder().build(hyper_rustls::HttpsConnectorBuilder::new().with_native_roots().https_or_http().enable_http1().build()),
        auth
    );
    let response = hub.files().list().doit().await.unwrap();
    let response_body = match serde_json::to_string(&response.1) {
        Ok(json) => json,
        Err(_) => "error".to_string(),
    };
    Ok(Response::builder().body(Full::new(Bytes::from(response_body))).unwrap())
}
async fn post(_request: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {
    Ok(Response::builder().body(Full::new(Bytes::from("Hello world on Post"))).unwrap())
}
