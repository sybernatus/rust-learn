use std::net::SocketAddr;

use http_body_util::Full;
use hyper::body::{Bytes, Incoming};
use hyper::server::conn::http1;
use hyper::service::service_fn;
use hyper::{Request, Response};
use hyper::http::Error;
use hyper_util::rt::TokioIo;
use tokio::net::TcpListener;
use crate::routing::routing;

mod routing;
mod map {
    pub mod routing;
}

async fn hello_world(request: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {

    fn make_response(string: String) -> Result<Response<Full<Bytes>>, Error> {
        Ok(Response::builder().body(Full::new(Bytes::from(string))).unwrap())
    }

    // let response = match (request.uri().path(), request.method()) {
    //     ("/", &Method::GET) => routing::,
    //     ("/test", &Method::GET) => make_response(format!("true test")),
    //     _ => make_response("oh no! not found".into()),
    // };

    let response = match request.uri().path() {
        "/" => routing(request).await,
        _ => make_response("oh no! not found".into()),
    };

    response
}

#[tokio::main]
async fn main() {

    let socket_address = SocketAddr::from(([127, 0, 0, 1], 3000));

    let listener = TcpListener::bind(socket_address).await.unwrap();

    loop {

        let (stream, _) = listener.accept().await.unwrap();
        let io = TokioIo::new(stream);
        tokio::task::spawn(async move {

            if let Err(err) = http1::Builder::new()
                .serve_connection(io, service_fn(hello_world))
                .await
            {
                println!("Error: {}", err);
            }

        });

    }
}