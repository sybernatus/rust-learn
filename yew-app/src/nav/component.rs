use yew::{function_component, Html, html};
use crate::nav::style::*;



#[function_component(Nav)]
pub fn nav() -> Html {

    html! {
        <div style={div_style()}>
            <nav style={nav_style()}>
                <a style={a_style()} href="/">{"Home"}</a>
                <a style={a_style()} href="/effect">{"Eff"}</a>
                <a style={a_style()} href="/comp">{"Comp"}</a>
                <a style={a_style()} href="/login/google">{"Login"}</a>
            </nav>
        </div>
    }
}
