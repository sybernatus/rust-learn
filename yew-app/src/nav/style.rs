pub fn div_style() -> &'static str {
    r"
    position: relative;
    "
}

pub fn nav_style() -> &'static str {
    r"
    // position: fixed;
    top: 0;
    left: 0;
    right: 0;
    height: 50px;
    display: flex;
    align-items: center;
    background: linear-gradient(to right, #b3b3b3, #808080);
    border-radius: 0.2rem;
    "
}
pub fn a_style() -> &'static str {
    r"
    flex: 1;
    border-right: 1px solid #ffffff;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    color: white;
    "
}