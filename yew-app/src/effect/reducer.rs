use std::rc::Rc;
use log::info;

use yew::Reducible;

use crate::effect::state::CounterState;

pub enum CounterAction {
    Decrement,
    Increment,
    AddIdToken(String)
}

#[derive(Clone)]
pub struct CounterReducer {
    pub state: CounterState,
}

impl Reducible for CounterReducer {
    type Action = CounterAction;

    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        let next_counter = match action {
            CounterAction::Decrement => self.state.count - 1,
            CounterAction::Increment => self.state.count + 1,
            _ => self.state.count
        };
        info!("CounterReducer::reduce: next_counter: {}", next_counter);

        let next_token = match action {
            CounterAction::AddIdToken(token) => token,
            _ => self.state.id_token.clone()
        };

        Self {
            state: CounterState {
                count: next_counter,
                id_token: next_token
            },
        }.into()
    }
}
impl Default for CounterReducer {
    fn default() -> Self {
        Self { state: CounterState::default() }
    }
}