
#[derive(Clone, PartialEq, Default)]
pub struct CounterState {
    pub count: i32,
    pub id_token: String
}