use yew::{Callback, function_component, Html, html, use_reducer, UseReducerHandle};

use crate::effect::reducer::{CounterAction, CounterReducer};

#[function_component(Eff)]
pub fn my_eff() -> Html {
    let counter_reducer: UseReducerHandle<CounterReducer> = use_reducer(CounterReducer::default);

    let increment_onclick = {
        let counter = counter_reducer.clone();
        Callback::from(move |_| counter.dispatch(CounterAction::Increment))
    };

    let decrement_onclick = {
        let counter = counter_reducer.clone();
        Callback::from(move |_| counter.dispatch(CounterAction::Decrement))
    };

    html! {
        <>
            <div>{ format!("Counter: {}", counter_reducer.state.count) }</div>

            <button onclick={increment_onclick}>{ "Increment" }</button>
            <button onclick={decrement_onclick}>{ "Decrement" }</button>
        </>
    }

}