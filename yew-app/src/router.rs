use yew::{Html, html};
use yew_router::prelude::*;

use crate::comp::component::Comp;
use crate::effect::component::Eff;
use crate::login::google::component::{LoginGoogle};
use crate::login::callback::component::{LoginCallback};

#[derive(Routable, Clone, Debug, PartialEq, Eq)]
pub enum Route {
    #[at("/comp")]
    Comp,
    #[at("/effect")]
    Eff,
    #[at("/login/google")]
    Google,
    #[at("/login/callback")]
    Callback,
}

pub fn switch(routes: Route) -> Html {
    match routes {
        Route::Comp => html!(<Comp />),
        Route::Eff => html!(<Eff />),
        Route::Google => html!(<LoginGoogle />),
        Route::Callback => html!(<LoginCallback />),
    }
}