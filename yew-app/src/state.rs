use yewdux::store::Store;
use serde::{Deserialize, Serialize};

#[derive(Clone, PartialEq, Eq, Deserialize, Serialize, Store, Default)]
#[store(storage = "local", storage_tab_sync)]
pub struct RootState {
    pub(crate) id_token: &'static str,
    pub(crate) other: String
}