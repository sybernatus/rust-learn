use yew::{function_component, Html, html};

#[function_component(LoginCallback)]
pub fn login_callback() -> Html {
    html! {
        <div>
            {"callback"}
        </div>
    }
}