use gloo_storage::{LocalStorage, Storage};
use js_sys::JSON::stringify;
use log::info;
use serde::{Deserialize, Serialize};
use wasm_bindgen::{JsCast, prelude::Closure};
use wasm_bindgen::prelude::wasm_bindgen;
use web_sys::{CustomEvent, HtmlElement};
use yew::{Component, Context, function_component, Html, html, NodeRef};
use yewdux::prelude::Dispatch;

use crate::reducer::RootMessages;
use crate::state::RootState;

#[wasm_bindgen]
extern "C" {
    fn trigger_custom_event();
}
#[function_component(LoginGoogle)]
pub fn login_google() -> Html {

    html! {
        <Google />
    }
}

struct Google {
    node_ref: NodeRef
}
pub enum GoogleMsg {
    GoogleEvent(GoogleCredentials)
}


#[derive(Serialize, Deserialize)]
pub struct GoogleCredentials {

    #[allow(non_snake_case)]
    clientId: String,
    client_id: String,
    credential: String,
}

impl Component for Google {
    type Message = GoogleMsg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            node_ref: NodeRef::default()
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            GoogleMsg::GoogleEvent(google_credentials) => {
                info!("Google event - {:?}", google_credentials.credential);
                let id_token = google_credentials.credential;
                LocalStorage::set("id_token", &id_token).expect("set google_credentials");
                let dispatch = Dispatch::<RootState>::new();
                dispatch.apply(RootMessages::UpdateIdToken(id_token));
                // let counter_reducer: UseReducerHandle<CounterReducer> = use_reducer(CounterReducer::default);
                // counter_reducer.dispatch(CounterAction::AddIdToken(id_token));
                true
            }
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <div>
                <script src="https://accounts.google.com/gsi/client" async=true defer=true></script>
                <script>{"\
                    function handle_credential_response(response) { \
                        let event = new CustomEvent('google-auth', { detail: response }); \
                        document.getElementById('g_id_onload').dispatchEvent(event); \
                        console.log('event - ', event); \
                }"}</script>
                <div id="g_id_onload" ref={self.node_ref.clone()}
                    data-client_id="1073985485921-3sskn8as78ta8slf4bed9j7sl9otjfu9.apps.googleusercontent.com"
                    data-callback="handle_credential_response">
                </div>
                <div class="g_id_signin" data-type="standard"></div>
            </div>
        }
    }
    fn rendered(&mut self, ctx: &Context<Self>, _first_render: bool) {
        let callback = ctx.link().callback(|event: CustomEvent| {
            info!("callback - {:?}", stringify(&event.detail()));
            let json_js_string = stringify(&event.detail()).expect("Error stringifying event detail");
            let json_string: String = json_js_string.into();
            GoogleMsg::GoogleEvent(serde_json::from_str(&json_string).expect("Error deserializing event detail"))
        });

        let node = self.node_ref.cast::<HtmlElement>().expect("Error casting node");
        let listener = Closure::<dyn Fn(CustomEvent)>::wrap(Box::new(move |event| {
            info!("closure - {:?}", event.detail());
            callback.emit(event);
        }));
        node.add_event_listener_with_callback("google-auth", listener.as_ref().unchecked_ref()).unwrap();
        listener.forget();
    }
}