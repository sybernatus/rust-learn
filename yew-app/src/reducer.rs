use std::ops::Deref;
use std::rc::Rc;

use yewdux::store::Reducer;

use crate::state::RootState;

pub enum RootMessages {
    UpdateIdToken(String)
}
impl Reducer<RootState> for RootMessages {
    fn apply(self, mut state: Rc<RootState>) -> Rc<RootState> {
        let state = Rc::make_mut(&mut state);
        match self {
            RootMessages::UpdateIdToken(token) =>  {
                state.id_token = token.as_str()
            },
        };

        state.deref().clone().into()
    }
}