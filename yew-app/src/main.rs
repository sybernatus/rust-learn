use yew::{Html, html};
use yew::prelude::function_component;
use yew_router::BrowserRouter;
use yew_router::prelude::Switch;

mod router;
mod style;
mod reducer;
mod state;

mod comp {
    pub mod component;
}
mod login {
    pub mod google {
        pub mod component;
    }
    pub mod callback {
        pub mod component;
    }
}
mod effect {
    pub mod component;
    pub mod state;
    pub mod reducer;
}

mod nav {
    pub mod component;
    pub mod style;
}

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <div>
            <nav::component::Nav />
            <BrowserRouter>
                <Switch<router::Route> render={router::switch} /> // <- must be child of <BrowserRouter>
            </BrowserRouter>
        </div>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::Renderer::<App>::new().render();
}