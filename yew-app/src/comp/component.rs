use std::io::Read;

use gloo_storage::LocalStorage;
use gloo_storage::Storage;
use log::{error, info};
use yew::{Component, Context, function_component, Hook, Html, html, Properties, ToHtml};
use yewdux::functional::use_selector;

#[derive(Clone, PartialEq, Properties)]
struct CompProperties {
    #[prop_or_default()]
    root: Option<&'static str>,
}

#[function_component(Comp)]
pub fn comp() -> Html {
    // let id_token = use_selector(|state: &RootState| state.id_token);
    // let _id_token = use_selector(|state: &RootState| state.id_token);
    // info!("id_token {}", *id_token);
    html! {
        <div>
            <p>{ "I'm a component" }</p>
            <TestButton my_prop="" />
        </div>
    }
}

use crate::state::RootState;

struct TestButton {
    count: u32,
    display_bitch: bool
}
enum CompMsg {
    ButtonClicked { string: &'static str },
    ButtonBitchClicked,
}

#[derive(Clone, PartialEq, Properties)]
struct TestButtonProperties {
    #[prop_or_default()]
    my_prop: Option<&'static str>,
}


impl Component for TestButton {
    type Message = CompMsg;
    type Properties = TestButtonProperties;

    fn create(_ctx: &Context<Self>) -> Self {
        LocalStorage::set("count", 0).expect("set count");
        Self { count: 0, display_bitch: false }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {


        fn button_click(string: &str) -> bool {
            info!("Button clicked {:?}", string);
            true
        }

        match msg {
            CompMsg::ButtonClicked {  string} => {
                self.count += 1;
                LocalStorage::set("count", self.count).expect("set count");
                button_click(string)
            },
            CompMsg::ButtonBitchClicked => {
                self.display_bitch = ! self.display_bitch;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let display_count: u32 = match LocalStorage::get("count") {
            Ok(count) => count,
            Err(_) => {
                error!("Error getting count");
                0
            }
        };
        let onclick = ctx.link().callback(|_| CompMsg::ButtonBitchClicked );

        html! {
            <div>
                <button onclick={ctx.link().callback(|_| CompMsg::ButtonClicked { string: "youhouuuuu" })}>{"Click me - "}{display_count}</button>
                <button {onclick}>{"Click me Bitch "}</button>
                {
                    if self.display_bitch {
                        html! {
                            <>
                            // <p>{ *id_token }</p>
                            <h1>{ "You clicked me Bitch!" }</h1>
                            </>
                        }
                    } else {
                        html! {}
                    }
                }
            </div>
        }
    }
}