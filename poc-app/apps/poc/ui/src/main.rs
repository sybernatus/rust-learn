use std::rc::Rc;
use yew::prelude::function_component;
use yew::{html, Html};
use yewdux::dispatch::Dispatch;

use libs_navigation_router_ui::LibsNavigationRouter;
use libs_navigation_top_bar::LibsNavigationTopBar;
use libs_auth_storage::{State as AuthState, StateMessages as AuthStateMessages};

#[function_component(App)]
pub fn app() -> Html {

    // let access_token = use_selector(|state: &AuthState| state.access_token_state.access_token.clone());
    let auth_state_dispatch = Dispatch::<AuthState>::new();
    let auth_state: Rc<AuthState> = auth_state_dispatch.get();
    html! {
        <div>
            <LibsNavigationTopBar />
            <p>{ "Access Token : "}{&auth_state.access_token_state.access_token}</p>
            <LibsNavigationRouter />
        </div>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::Renderer::<App>::new().render();
}
