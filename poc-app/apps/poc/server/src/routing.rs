use http_body_util::Full;
use hyper::body::{Bytes, Incoming};
use hyper::http::{Error, HeaderValue};
use hyper::{HeaderMap, Request, Response, StatusCode};

pub async fn routing(request: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {
    let response: Result<Response<Full<Bytes>>, Error>;
    let root_path = request.uri().path();

    if root_path.starts_with("/google/drive/v3/api") {
        response = libs_google_drive_v3_api::routing(request, "/google/drive/v3/api").await;
    } else if root_path.starts_with("/google/calendar/v3/api") {
        response = libs_google_calendar_v3_api::routing(request, "/google/calendar/v3/api").await;
    } else if root_path.starts_with("/game/platformer/server") {
        response = libs_game_platformer_server::routing(request, "/game/platformer/server").await;
    } else {
        response = Response::builder()
            .header("Access-Control-Allow-Origin", "*")
            .status(StatusCode::NOT_FOUND)
            .body(Full::new(Bytes::from("root path not found")))
    }
    let mut response = response.unwrap();
    set_headers_response(response.headers_mut()).await;
    Ok(response)
}


async fn set_headers_response(headers: &mut HeaderMap<HeaderValue>) {
    headers.insert(
        "Access-Control-Allow-Origin",
        HeaderValue::from_str("*").expect("Unable to set header"),
    );
    headers.insert(
        "Access-Control-Allow-Methods",
        HeaderValue::from_str("GET, POST, OPTIONS").expect("Unable to set header"),
    );
    headers.insert(
        "Access-Control-Allow-Headers",
        HeaderValue::from_str("Content-Type").expect("Unable to set header"),
    );
}

#[cfg(test)]
mod tests {
    #[tokio::test]
    async fn test_headers() {
        let mut headers = hyper::HeaderMap::new();
        super::set_headers_response(&mut headers).await;
        assert_eq!(
            headers.get("Access-Control-Allow-Origin").unwrap(),
            &hyper::header::HeaderValue::from_static("*")
        );
        assert_eq!(
            headers.get("Access-Control-Allow-Methods").unwrap(),
            &hyper::header::HeaderValue::from_static("GET, POST, OPTIONS")
        );
        assert_eq!(
            headers.get("Access-Control-Allow-Headers").unwrap(),
            &hyper::header::HeaderValue::from_static("Content-Type")
        );

    }
}