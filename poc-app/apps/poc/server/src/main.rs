use std::net::SocketAddr;

use hyper::server::conn::http1;
use hyper::service::service_fn;
use hyper_util::rt::TokioIo;
use tokio::net::TcpListener;

use crate::routing::routing;

pub mod routing;

#[tokio::main]
async fn main() {
    let socket_address = SocketAddr::from(([127, 0, 0, 1], 3000));

    let listener = TcpListener::bind(socket_address).await.unwrap();
    loop {
        let (stream, _) = listener.accept().await.unwrap();
        let io = TokioIo::new(stream);
        tokio::task::spawn(async move {
            if let Err(err) = http1::Builder::new()
                .serve_connection(io, service_fn(routing))
                .await
            {
                println!("Error: {}", err);
            }
        });
    }
}
