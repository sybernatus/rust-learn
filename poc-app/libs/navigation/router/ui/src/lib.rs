use yew::{function_component, html, Html};
use yew_router::prelude::*;

use libs_auth_google_pop_up::LibsAuthGooglePopUp;
use libs_auth_login_callback::LibsAuthLoginCallback;
use libs_auth_logout::LibsAuthLogout;
use libs_game_godot::LibsGameGodot;

#[function_component(LibsNavigationRouter)]
pub fn lib() -> Html {
    html! {
        <>
            <BrowserRouter>
                <Switch<Route> render={switch} />
            </BrowserRouter>
        </>
    }
}

#[derive(Routable, Clone, Debug, PartialEq, Eq)]
pub enum Route {
    #[at("/comp")]
    Comp,
    #[at("/effect")]
    Eff,
    #[at("/login/google")]
    AuthLoginGoogleSignIn,
    #[at("/login/callback")]
    LoginCallback,
    #[at("/game/godot")]
    GameGodot,
    #[at("/logout")]
    Logout,
}

pub fn switch(routes: Route) -> Html {
    match routes {
        Route::AuthLoginGoogleSignIn => html!(<LibsAuthGooglePopUp />),
        Route::LoginCallback => html!(<LibsAuthLoginCallback />),
        Route::GameGodot => html!(<LibsGameGodot />),
        Route::Logout => html!(<LibsAuthLogout />),
        _ => html!(<>{"Nothing to show"}</>),
    }
}
