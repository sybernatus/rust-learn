use yew::{html, Html};

pub fn style() -> Html {
    html! {

        <style>
            { "
                .libs-navigation-top-bar {
                    & > div {
                        position: relative;
                    }

                    & > nav {
                        top: 0;
                        left: 0;
                        right: 0;
                        height: 30px;
                        display: flex;
                        align-items: center;
                        background: linear-gradient(to right, #b3b3b3, #808080);
                        border-radius: 0.2rem;

                        & > a {
                            flex: 1;
                            border-left: 1px solid #ffffff;
                            border-right: 1px solid #ffffff;
                            text-align: center;
                            padding: 6px 8px;
                            text-decoration: none;
                            color: white;
                            &:hover {
                                background-color: #555;
                                color: white;
                                transform: scale(0.95);
                                transition: all 0.3s ease;
                            }
                            & > img {
                                width: 25px;
                                height: 25px;
                                border-radius: 50%;
                                margin-left: 10px;
                            }
                        }
                    }
                }
            " }
        </style>
    }
}
