use yew::{classes, function_component, html, Component, Context, Html};

use crate::style::style;

mod style;

#[function_component(LibsNavigationTopBar)]
pub fn lib() -> Html {
    html! {
        <>
            <NavigationTopBar />
        </>
    }
}

struct NavigationTopBar {}

impl Component for NavigationTopBar {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        NavigationTopBar {}
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        true
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <>
                { style() }
                <div class={classes!("libs-navigation-top-bar")}>
                    <nav>
                        <a href="/">{"Home"}</a>
                        <a href="/effect">{"Eff"}</a>
                        <a href="/comp">{"Comp"}</a>
                        <a href="/game/godot">{"Game"}</a>
                        {
                            if ! libs_auth_token::is_authenticated() {
                                html! {
                                    <a href="/login/google">{"Login"}</a>
                                }
                            } else {
                                html! {
                                    <a href="/logout">{"Logout"}
                                        <img src={libs_auth_token::id_token_claim().picture} />
                                    </a>
                                }
                            }
                        }
                    </nav>
                </div>
            </>
        }
    }
}
