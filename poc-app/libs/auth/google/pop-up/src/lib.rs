use core::option::Option;
use std::borrow::Cow;

use gloo_storage::{LocalStorage, Storage};
use gloo_utils::window;
use js_sys::JSON::stringify;
use log::{debug, info};
use oauth2::basic::BasicClient;
use oauth2::{
    AuthUrl, ClientId, ClientSecret, CsrfToken, PkceCodeChallenge, RedirectUrl, Scope, TokenUrl,
};
use serde::{Deserialize, Serialize};
use wasm_bindgen::{prelude::Closure, JsCast};
use web_sys::{CustomEvent, HtmlElement};
use yew::platform::spawn_local;
use yew::{function_component, html, Component, Context, Html, NodeRef};
use yewdux::dispatch::Dispatch;

use libs_auth_storage::{State as AuthState, StateMessages as AuthStateMessages};

#[function_component(LibsAuthGooglePopUp)]
pub fn lib() -> Html {
    html! {
        <GooglePopUp />
    }
}

struct GooglePopUp {
    node_ref: NodeRef,
}
pub enum GooglePopUpMessages {
    GoogleEvent(GoogleCredentials),
}

#[derive(Serialize, Deserialize)]
pub struct GoogleCredentials {
    client_id: String,
    credential: String,
}

impl Component for GooglePopUp {
    type Message = GooglePopUpMessages;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            node_ref: NodeRef::default(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            GooglePopUpMessages::GoogleEvent(google_credentials) => {
                info!("Google event - {:?}", google_credentials.credential);
                let id_token = google_credentials.credential;
                LocalStorage::set("id_token", id_token.clone().as_str())
                    .expect("set google_credentials");
                let dispatch = Dispatch::<AuthState>::new();
                dispatch.apply(AuthStateMessages::UpdateIdToken(id_token));
                let _token = spawn_local(request_access_token());
                true
            }
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        // let id_token = use_selector(|state: &AuthState| state.id_token.clone());
        // format!("id_token: {}", id_token);
        html! {
            <div>
                <script src="https://accounts.google.com/gsi/client" async=true defer=true></script>
                <script>{"\
                    function handle_credential_response(response) { \
                        let event = new CustomEvent('google-auth', { detail: response }); \
                        document.getElementById('g_id_onload').dispatchEvent(event); \
                        console.log('event - ', event); \
                }"}</script>
                <div id="g_id_onload" ref={self.node_ref.clone()}
                    data-client_id="1073985485921-3sskn8as78ta8slf4bed9j7sl9otjfu9.apps.googleusercontent.com"
                    data-callback="handle_credential_response">
                </div>
                <div class="g_id_signin" data-type="standard"></div>
            </div>
        }
    }
    fn rendered(&mut self, ctx: &Context<Self>, _first_render: bool) {
        let callback = ctx.link().callback(|event: CustomEvent| {
            debug!("callback - {:?}", stringify(&event.detail()));
            let json_js_string =
                stringify(&event.detail()).expect("Error stringifying event detail");
            let json_string: String = json_js_string.into();
            GooglePopUpMessages::GoogleEvent(
                serde_json::from_str(&json_string).expect("Error deserializing event detail"),
            )
        });

        let node = self
            .node_ref
            .cast::<HtmlElement>()
            .expect("Error casting node");
        let listener = Closure::<dyn Fn(CustomEvent)>::wrap(Box::new(move |event| {
            debug!("closure - {:?}", event.detail());
            callback.emit(event);
        }));
        node.add_event_listener_with_callback("google-auth", listener.as_ref().unchecked_ref())
            .unwrap();
        listener.forget();
    }
}

async fn request_access_token() {
    let client_id = ClientId::new(
        "1073985485921-3sskn8as78ta8slf4bed9j7sl9otjfu9.apps.googleusercontent.com".to_string(),
    );
    let client_secret = Option::from(ClientSecret::new(
        "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".to_string(),
    ));
    let auth_url = AuthUrl::new("https://accounts.google.com/o/oauth2/auth".to_string())
        .expect("Error creating auth url");
    let token_url = Option::from(
        TokenUrl::new("https://oauth2.googleapis.com/token".to_string())
            .expect("Error creating token url"),
    );
    let redirect_url = RedirectUrl::new("http://localhost:8080/login/callback".to_string())
        .expect("Error creating redirect url");
    let client = BasicClient::new(client_id, client_secret, auth_url, token_url);

    // Generate a PKCE challenge.1
    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

    LocalStorage::set("pkce_verifier", pkce_verifier.secret())
        .expect("Unable to store pkce_verifier");
    // Generate the full authorization URL.
    let (auth_url, _csrf_token) = client
        .authorize_url(CsrfToken::new_random)
        // Set the desired scopes.
        .add_scopes(vec![
            Scope::new("https://www.googleapis.com/auth/calendar".to_string()),
            Scope::new("https://www.googleapis.com/auth/calendar.events.readonly".to_string()),
            Scope::new("https://www.googleapis.com/auth/calendar.readonly".to_string()),
            Scope::new("https://www.googleapis.com/auth/drive.readonly".to_string()),
            Scope::new("https://www.googleapis.com/auth/drive.file".to_string()),
            Scope::new("https://www.googleapis.com/auth/drive.metadata".to_string()),
        ])
        // Set the PKCE code challenge.
        .set_pkce_challenge(pkce_challenge)
        .set_redirect_uri(Cow::Borrowed(&redirect_url))
        .add_extra_param("pkce_verifier", pkce_verifier.secret())
        .url();

    window()
        .location()
        .set_href(auth_url.as_str())
        .expect("should redirect");
}
