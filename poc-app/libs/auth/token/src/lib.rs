use gloo_storage::{LocalStorage, Storage};
use jwt_compact::UntrustedToken;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct IdTokenClaim {
    pub picture: String,
}

pub fn is_authenticated() -> bool {
    match LocalStorage::get::<String>("id_token") {
        Ok(_) => true,
        Err(_) => false,
    }
}
pub fn id_token_claim() -> IdTokenClaim {
    let mut picture = String::from("");
    if is_authenticated() {
        let id_token: String =
            LocalStorage::get("id_token").expect("id_token_claim - Error getting id_token");
        let untrusted_token = UntrustedToken::new(id_token.as_str())
            .expect("id_token_claim - Error creating untrusted_token");
        let claim: jwt_compact::Claims<IdTokenClaim> = untrusted_token
            .deserialize_claims_unchecked()
            .expect("id_token_claim - Error deserializing claims");
        picture = claim.custom.picture;
    }

    IdTokenClaim { picture }
}

