use gloo_storage::{LocalStorage, Storage};
use gloo_utils::window;
use yew::{function_component, html, Html};

#[function_component(LibsAuthLogout)]
pub fn lib() -> Html {
    LocalStorage::delete("id_token");
    window()
        .location()
        .replace("/")
        .expect("Error setting href");
    html! {
        <>{"Logout"}</>
    }
}
