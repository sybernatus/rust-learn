use std::rc::Rc;
use std::time::Duration;

use oauth2::basic::BasicTokenResponse;
use oauth2::{Scope, TokenResponse};
use serde::{Deserialize, Serialize};
use yewdux::store::{Reducer, Store};

use StateMessages::UpdateAccessToken;

#[derive(Default, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct AccessTokenState {
    pub access_token: String,
    pub expires_in: Duration,
    pub token_type: String,
    pub scope: Vec<Scope>,
}

#[derive(Clone, PartialEq, Eq, Deserialize, Serialize, Store, Default)]
#[store(storage = "local", storage_tab_sync)]
pub struct State {
    pub id_token: String,
    pub access_token_state: AccessTokenState,
}

pub enum StateMessages {
    UpdateIdToken(String),
    UpdateAccessToken(BasicTokenResponse),
}

impl Reducer<State> for StateMessages {
    fn apply(self, mut state: Rc<State>) -> Rc<State> {
        let state = Rc::make_mut(&mut state);
        match self {
            StateMessages::UpdateIdToken(token) => state.id_token = token,
            UpdateAccessToken(access_token_state) => {
                let new_access_token = access_token_state.access_token().secret().to_string();
                let new_expires_in = access_token_state
                    .expires_in()
                    .unwrap_or(Duration::new(0, 0));
                let new_token_type = access_token_state.token_type().clone();
                let new_scope = access_token_state.scopes().unwrap().to_vec();
                state.access_token_state = AccessTokenState {
                    access_token: new_access_token,
                    expires_in: new_expires_in,
                    token_type: new_token_type.as_ref().to_string(),
                    scope: new_scope,
                }
            }
        };

        state.clone().into()
    }
}
