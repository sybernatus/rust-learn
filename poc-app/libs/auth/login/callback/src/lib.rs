use core::option::Option;
use std::borrow::Cow;
use std::str::FromStr;

use gloo_storage::{LocalStorage, Storage};
use gloo_utils::window;
use log::info;
use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::url::Url;
use oauth2::{
    AuthUrl, AuthorizationCode, ClientId, ClientSecret, RedirectUrl, TokenResponse, TokenUrl,
};
use yew::platform::spawn_local;
use yew::{function_component, html, Component, Context, Html};
use yewdux::dispatch::Dispatch;

use libs_auth_storage::{State as AuthState, StateMessages as AuthStateMessages};

#[function_component(LibsAuthLoginCallback)]
pub fn lib() -> Html {
    html! {
        <LoginCallback />
    }
}

struct LoginCallback {}
pub enum LoginCallbackMessages {}

#[derive(Clone, PartialEq, Default)]
struct LoginCallbackQueryParams {
    scope: String,
    code: String,
    state: String,
}
impl Component for LoginCallback {
    type Message = LoginCallbackMessages;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        true
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        let query_params = query_params();
        spawn_local(request_access_token(query_params));

        window()
            .location()
            .replace("/")
            .expect("Error setting href");

        html! {
            <>
                <div>{"LoginCallback"}</div>
            </>
        }
    }
}

fn query_params() -> LoginCallbackQueryParams {
    let query_pairs = match window().location().href() {
        Ok(url) => {
            let url_str = url.as_str();
            let url = Url::parse(url_str).expect("parse query string").clone();
            url
        }
        Err(_) => Url::from_str("").expect("parse query string"),
    };
    let param = query_pairs.query_pairs();

    let state = match param.clone().find(|(key, _)| key == "state") {
        Some(cow_pairs) => cow_pairs.1.to_string(),
        None => "".to_string(),
    };
    info!("state - {:?}", state);

    let scope = match param.clone().find(|(key, _)| key == "scope") {
        Some(cow_pairs) => cow_pairs.1.to_string(),
        None => "".to_string(),
    };
    info!("scope - {:?}", scope);

    let code = match param.clone().find(|(key, _)| key == "code") {
        Some(cow_pairs) => cow_pairs.1.to_string(),
        None => "".to_string(),
    };
    info!("code - {:?}", code);

    LoginCallbackQueryParams { scope, code, state }
}

async fn request_access_token(login_callback_query_params: LoginCallbackQueryParams) {
    let client_id = ClientId::new(
        "1073985485921-3sskn8as78ta8slf4bed9j7sl9otjfu9.apps.googleusercontent.com".to_string(),
    );
    let client_secret = Option::from(ClientSecret::new(
        "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".to_string(),
    ));
    let auth_url = AuthUrl::new("https://accounts.google.com/o/oauth2/auth".to_string())
        .expect("Error creating auth url");
    let token_url = Option::from(
        TokenUrl::new("https://oauth2.googleapis.com/token".to_string())
            .expect("Error creating token url"),
    );
    let redirect_url = RedirectUrl::new("http://localhost:8080/login/callback".to_string())
        .expect("Error creating redirect url");
    let client = BasicClient::new(client_id, client_secret, auth_url, token_url);
    // Generate a PKCE challenge.1

    let pkce_verifier: String =
        LocalStorage::get("pkce_verifier").expect("Error getting pkce_verifier");

    info!(
        "login_callback_query_params.code - {:?}",
        login_callback_query_params.code.clone()
    );
    let access_token = client
        .exchange_code(AuthorizationCode::new(login_callback_query_params.code))
        .add_extra_param("code_verifier", pkce_verifier)
        .set_redirect_uri(Cow::Borrowed(&redirect_url))
        .request_async(async_http_client)
        .await
        .expect("Error requesting access token");

    info!("access_token - {:?}", access_token);

    LocalStorage::set("access_token", access_token.access_token().secret())
        .expect("Error storing access_token");
    let dispatch = Dispatch::<AuthState>::new();
    dispatch.apply(AuthStateMessages::UpdateAccessToken(access_token));
}
