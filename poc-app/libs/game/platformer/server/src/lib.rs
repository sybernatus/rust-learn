use http_body_util::Full;
use hyper::body::{Bytes, Incoming};
use hyper::http::Error;
use hyper::{Method, Request, Response, StatusCode};

pub async fn routing(
    request: Request<Incoming>,
    root_path: &str,
) -> Result<Response<Full<Bytes>>, Error> {
    let path = request.uri().path().replace(root_path, "");
    let method = request.method();
    if path == "" || path == "/" && method == Method::GET {
        get().await
    } else {
        Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(Full::new(Bytes::from("Platformer Server - not found")))
    }
}
async fn get() -> Result<Response<Full<Bytes>>, Error> {
    Response::builder()
        .status(StatusCode::OK)
        .body(Full::new(Bytes::from("<p>toto</p>")))
}
