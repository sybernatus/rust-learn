use log::info;
use web_sys::HtmlElement;
use yew::platform::spawn_local;
use yew::{function_component, html, use_effect, BaseComponent, Component, Context, Html, NodeRef};

#[function_component(LibsGameGodot)]
pub fn lib() -> Html {
    html! {
        <>
        {"Game"}
        <Platformer2D />
        {"Game end"}
        </>
    }
}

struct Platformer2D {
    node_ref: NodeRef,
    node_ref_list: Vec<NodeRef>,
    game_html: String,
    html_content: String,
}

pub enum Platformer2DMsg {
    FetchHtml,
    SetHtmlContent(String),
}
impl Component for Platformer2D {
    type Message = Platformer2DMsg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        let game_html = include_str!("../assets/single-player-game/Platformer 2D.html").to_string();
        _ctx.link().send_message(Platformer2DMsg::FetchHtml);
        Self {
            node_ref: NodeRef::default(),
            node_ref_list: vec![],
            game_html,
            html_content: "".to_string(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Platformer2DMsg::FetchHtml => {}
            Platformer2DMsg::SetHtmlContent(content) => {
                // Set your HTML content
                self.html_content = content;
            }
        }
        true
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html!(
            <>
                {self.html_content.clone()}
            </>
        )
    }

    fn rendered(&mut self, _ctx: &Context<Self>, _first_render: bool) {
        use_effect(move || {
            spawn_local(async move {
                let client = reqwest::Client::new();
                let response = client
                    .get("http://localhost:3000/game/platformer/server/")
                    .header("Accept", "text/html")
                    .send()
                    .await
                    .unwrap();
                Platformer2DMsg::SetHtmlContent(response.text().await.unwrap());
            });
        });
    }

    // fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
    //     if first_render {
    //         // document()
    //         // let window = window().expect("Cannot get window");
    //         // let document= document();
    //         // let view = self.view::<Platformer2D>();
    //
    //         // let game_html = html!("../assets/single-player-game/Platformer 2D.html");
    //
    //         // info!("view: {:?}", view);
    //         let scripts = document().get_elements_by_tag_name("script");
    //         let scripts_col: HtmlCollection = scripts.into();
    //         let js_src = include_str!("../assets/single-player-game/Platformer 2D.js");
    //         eval(js_src).expect("Error: cannot eval script");
    //
    //         for i in 0..scripts_col.length() {
    //             if let Some(script) = scripts_col.get_with_index(i) {
    //                 info!("script: {:?}", script.text_content());
    //                 let script_element = script.dyn_into::<HtmlScriptElement>().expect("Error: cannot cast script element");
    //                 info!("script_element: {:?}", script_element);
    //                 if let text = script_element.text().expect("Error: cannot get script text") {
    //                     if text.trim() != "" {
    //                         eval(&text)
    //                             .expect("Failed to execute script.");
    //                     }
    //                 }
    //                 // eval(script.text_content().expect("Error: no content in the script").as_str()).expect("Error: cannot eval script");
    //             }
    //             // let script = scripts_col.get_with_index(i).unwrap();
    //             // info!("script: {:?}", script.text_content());
    //             // eval(script.text_content().expect("Error: no content in the script").as_str()).expect("Error: cannot eval script");
    //             // let node = script.dyn_into::<Node>().unwrap();
    //             // info!("node: {:?}", node);
    //             // let node_ref = NodeRef::new(node);
    //             // info!("node_ref: {:?}", node_ref);
    //             // self.node_ref_list.push(node_ref);
    //         }
    //         let js_src = include_str!("../assets/single-player-game/coi-serviceworker.js");
    //         eval(js_src).expect("Error: cannot eval script");
    //         // for script in scripts_col {}
    //
    //         // let s = self.view().iter().next().unwrap();
    //         // info!("s: {:?}", s);
    //         // js_sys::Array::from(&scripts).for_each(&mut |script, _, _| {
    //         //     info!("scripts: {:?}", script.as_string());
    //         // });
    //         // let node = self.node_ref.cast::<HtmlElement>().unwrap();
    //         // let parent = document_element().parent_element().expect("Cannot get parent element");
    //         // parent.append_child(&node).unwrap();
    //         // execute_scripts(&self.node_ref.cast::<HtmlElement>().expect("Cannot get node"));
    //     }
    // }
}

fn execute_scripts(node: &HtmlElement) {
    let scripts = node.text_content().expect("Cannot get node");
    // .unwrap_throw()
    // .iter()
    // .filter_map(|n| n.dyn_into::<HtmlScriptElement>().ok());
    info!("scripts: {:?}", scripts);
    // for script in scripts.item(0) {
    //     info!("script: {:?}", script.text_content());
    // }
    // for script in scripts {
    //     if let Some(text) = script.text() {
    //         if text.trim() != "" {
    //             window()
    //                 .unwrap_throw()
    //                 .eval(&text)
    //                 .expect("Failed to execute script.");
    //         }
    //     }
    //
    //     if let Some(src) = script.src() {
    //         if src.trim() != "" {
    //             let new_script = document()
    //                 .create_element("script")
    //                 .unwrap_throw()
    //                 .dyn_into::<HtmlScriptElement>()
    //                 .unwrap_throw();
    //
    //             new_script.set_type("text/javascript");
    //             new_script.set_src(&src);
    //             document()
    //                 .head()
    //                 .unwrap_throw()
    //                 .append_child(&new_script)
    //                 .unwrap_throw();
    //         }
    //     }
    // }
}
