use core::option::Option;
use std::borrow::Cow;
use std::str::FromStr;

use gloo_storage::{LocalStorage, Storage};
use gloo_utils::window;
use log::info;
use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::url::Url;
use oauth2::{
    AuthUrl, AuthorizationCode, ClientId, ClientSecret, RedirectUrl, TokenResponse, TokenUrl,
};
use yew::platform::spawn_local;
use yew::{function_component, html, Component, Context, Html};
use yewdux::dispatch::Dispatch;

use libs_auth_storage::{State as AuthState, StateMessages as AuthStateMessages};

#[function_component(LibsBackendsGoogleApiDrive)]
pub fn lib() -> Html {
    html! {
        <div />
    }
}

struct LibsBackendsGoogleApiDrive {}
pub enum LibsBackendsGoogleApiDriveMessages {}

#[derive(Clone, PartialEq, Default)]
struct LibsBackendsGoogleApiDriveQueryParams {
    scope: String,
    code: String,
    state: String,
}
impl Component for LibsBackendsGoogleApiDrive {
    type Message = LibsBackendsGoogleApiDriveMessages;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        true
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        let query_params = query_params();
        spawn_local(request_access_token(query_params));

        html! {
            <>
                <div>{"LibsBackendsGoogleApiDrive"}</div>
            </>
        }
    }
}