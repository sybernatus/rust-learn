use google_drive3::hyper::Client;
use google_drive3::{hyper_rustls, oauth2, DriveHub};
use http_body_util::Full;
use hyper::body::{Bytes, Incoming};
use hyper::http::Error;
use hyper::{Method, Request, Response, StatusCode};
use std::collections::HashMap;

pub async fn routing(
    request: Request<Incoming>,
    root_path: &str,
) -> Result<Response<Full<Bytes>>, Error> {
    let path = request.uri().path().replace(root_path, "");
    let method = request.method();
    if path == "" || path == "/" && method == Method::GET {
        get().await
    } else if path == "/list" || path == "/list/" && method == Method::GET {
        list(request).await
    } else {
        Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(Full::new(Bytes::from("Google API - not found")))
    }
}

async fn get() -> Result<Response<Full<Bytes>>, Error> {
    Response::builder()
        .status(StatusCode::OK)
        .body(Full::new(Bytes::from("Google API - get")))
}

async fn list(request: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {
    let query = if let Some(query_parameters) = request.uri().query() {
        query_parameters
    } else {
        return Ok(Response::builder()
            .status(StatusCode::UNPROCESSABLE_ENTITY)
            .body(Full::new(Bytes::from("query parameters not found")))
            .unwrap());
    };
    let params = form_urlencoded::parse(query.as_bytes())
        .into_owned()
        .collect::<HashMap<String, String>>();
    let access_token = if let Some(p) = params.get("access_token") {
        p.as_str().to_string()
    } else {
        return Ok(Response::builder()
            .status(StatusCode::UNPROCESSABLE_ENTITY)
            .body(Full::new(Bytes::from(
                "query parameters - 'access_token' not found",
            )))
            .unwrap());
    };
    let auth = oauth2::AccessTokenAuthenticator::builder(access_token)
        .build()
        .await
        .unwrap();
    let hub = DriveHub::new(
        Client::builder().build(
            hyper_rustls::HttpsConnectorBuilder::new()
                .with_native_roots()
                .https_or_http()
                .enable_http1()
                .build(),
        ),
        auth,
    );
    let response = hub.files().list().doit().await.unwrap();
    let response_body = match serde_json::to_string(&response.1) {
        Ok(json) => json,
        Err(_) => "error".to_string(),
    };
    Response::builder().body(Full::new(Bytes::from(response_body)))
}
