use google_calendar3::CalendarHub;
use google_drive3::{hyper_rustls, oauth2};
use google_drive3::hyper::Client;
use http_body_util::Full;
use hyper::{HeaderMap, Method, Request, Response, StatusCode};
use hyper::body::{Bytes, Incoming};
use hyper::http::Error;

pub async fn routing(
    request: Request<Incoming>,
    root_path: &str,
) -> Result<Response<Full<Bytes>>, Error> {
    let path = request.uri().path().replace(root_path, "");
    let method = request.method();
    if path == "" || path == "/" && method == Method::GET {
        get().await
    } else if path == "/users/me/calendarList" || path == "/users/me/calendarList/" && method == Method::GET {
        calendar_list(request).await
    } else {
        Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(Full::new(Bytes::from("Google API - not found")))
    }
}

async fn get() -> Result<Response<Full<Bytes>>, Error> {
    Response::builder()
        .status(StatusCode::OK)
        .body(Full::new(Bytes::from("Google API - get calendars")))
}

async fn calendar_list(request: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {
    let access_token = extract_authorization_token(request.headers().clone()).await;
    let access_token = match access_token {
        Ok(access_token) => access_token,
        Err(error) => {
            return Ok(Response::builder()
                .status(StatusCode::UNPROCESSABLE_ENTITY)
                .body(Full::new(Bytes::from(error)))
                .unwrap());
        }
    };

    println!("access token {}", &access_token);

    let auth = oauth2::AccessTokenAuthenticator::builder(access_token.to_string())
        .build()
        .await
        .unwrap();
    let hub = CalendarHub::new(
        Client::builder().build(
            hyper_rustls::HttpsConnectorBuilder::new()
                .with_native_roots()
                .https_or_http()
                .enable_http1()
                .build(),
        ),
        auth,
    );
    let response = match hub.calendar_list().list().doit().await {
        Ok(response) => response,
        Err(err) => {
            return Ok(Response::builder()
                .status(StatusCode::UNAUTHORIZED)
                .body(Full::new(Bytes::from(err.to_string()))).unwrap());
        }
    };
    let response_body = match serde_json::to_string(&response.1) {
        Ok(json) => json,
        Err(_) => "error".to_string(),
    };
    Response::builder().body(Full::new(Bytes::from(response_body)))
}

async fn extract_authorization_token(headers: HeaderMap) -> Result<String, &'static str> {
    let access_token = if let Some(access_token) = headers.get("Authorization") {
        access_token.to_str()
    } else {
        return Err("Authorization not found");
    };

    let access_token = match access_token {
        Ok(access_token) => access_token.replace("Bearer ", ""),
        Err(_) => {
            return Err("Authorization not found");
        }
    };

    Ok(access_token)
}